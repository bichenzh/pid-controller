`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: Duke University
// Engineer: Rachel M. Noek
// 
// Create Date:    13:25:22 03/11/2019 
// Design Name: 
// Module Name:    dac_ramp 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module dac_ramp #(
    parameter N_DAC = 8,       // Number of output channels
	 parameter W_DAC_CHAN = 3,  // Width of DAC write channel
	 parameter W_DAC_DATA = 16,
    parameter W_WR_ADDR = 16,   // Width of memory write address
    parameter W_WR_CHAN = 16,   // Width of memory write channel
    parameter W_WR_DATA = 48    // Width of memory write data
    )(
    input wire clk_in,
	 input wire sys_rst,
	 input wire wr_en,
    input wire [W_WR_ADDR-1:0] wr_addr,
    input wire [W_WR_CHAN-1:0] wr_chan,
    input wire [W_WR_DATA-1:0] wr_data,
    output wire [W_DAC_CHAN-1:0] chan_out,
    output wire [W_DAC_DATA-1:0] data_out,
    output wire valid
    );

`include "ep_map.vh"

reg  [W_DAC_DATA-1:0] dac_min_val[0:N_DAC-1];
reg  [W_DAC_DATA-1:0] dac_max_val[0:N_DAC-1];
reg  [15:0] cycles_per_step[0:N_DAC-1];
reg  [W_DAC_DATA-1:0] dac_static_val[0:N_DAC-1];
reg  [15:0]  counter[0:N_DAC-1];
reg  [W_DAC_DATA-1:0] current_dac_val[0:N_DAC-1];
reg  [W_DAC_DATA-1:0] next_dac_val[0:N_DAC-1];
reg  [N_DAC-1:0] direction; // going up? 1 = up, 0 = down
reg  [W_DAC_CHAN-1:0] current_chan[0:N_DAC-1];
reg  [W_DAC_DATA-1:0] current_data[0:N_DAC-1];
reg  [W_DAC_CHAN-1:0] chan;
reg  [W_DAC_DATA-1:0] data;
reg  [N_DAC-1:0] chan_valid;
// for the fifos: -----------
wire [N_DAC-1:0] ramp_buf_rd_en;
wire [W_DAC_CHAN-1:0] buf_ramp_chan[0:N_DAC-1];
wire [W_DAC_DATA-1:0] buf_ramp_data[0:N_DAC-1];
wire [N_DAC-1:0] buf_ramp_dv;
wire [N_DAC-1:0] ramp_fifo_empty;
wire [N_DAC-1:0] ramp_fifo_full;
// --------------------------
reg [W_DAC_CHAN-1:0] buf_chan;
reg [W_DAC_DATA-1:0] buf_data;
reg [N_DAC-1:0] ramp_enable = 0;
reg data_valid;


reg  [15:0] step_size[0:N_DAC-1];


integer i;
initial begin
    for ( i = 0; i < N_DAC; i = i + 1 ) begin 
        dac_min_val[i] <= 16'b0000000000000000;
		  dac_max_val[i] <= 16'b1111111111111111;
		  dac_static_val[i] <= 0;
		  current_dac_val[i] <= 0;
		  next_dac_val[i] <= 0;
		  cycles_per_step[i] <= 16'b0000000011111000; //248 default
		  counter[i] <= 0;
		  direction[i] <= 1'b1; // start by going up.
		  step_size[i] <= 16'b0000000001000001;//65    default
    end
end


always @( posedge clk_in ) begin
    if ( wr_en && (wr_chan < N_DAC) ) begin
        case ( wr_addr )
				opt_min_addr : dac_min_val[wr_chan] <= wr_data[15:0];
            opt_max_addr : dac_max_val[wr_chan] <= wr_data[15:0];
				opt_init_addr : dac_static_val[wr_chan] <= wr_data[15:0];
				opt_cyc_per_step : cycles_per_step[wr_chan] <= wr_data[15:0];
				opt_ramp_step_size : step_size[wr_chan] <= wr_data[15:0];
            dac_ramp_en_rqst : ramp_enable[wr_chan] <= 1'b1;
            dac_ramp_dis_rqst : ramp_enable[wr_chan] <= 1'b0;
        endcase
    end
end


genvar m;
generate
	for (m = 0; m < N_DAC; m = m + 1) begin: ramp_fifos
		dac_ramp_fifo ramp_buf(
			 .wr_clk (clk_in),
			 .rd_clk (clk_in),
			 .rst    (sys_rst),
			 .din    ({current_chan[m], current_data[m]}),
			 .wr_en  (chan_valid[m]),
			 .rd_en  (ramp_buf_rd_en[m]),
			 .dout   ({buf_ramp_chan[m], buf_ramp_data[m]}),
			 .valid  (buf_ramp_dv[m]),
			 .empty  (ramp_fifo_empty[m]),
			 .full 	(ramp_fifo_full[m])
			 );
	end
endgenerate
// the state machine for moving things out of the fifos to the main controller
localparam [3:0] MOVE_DATA = 4'h0;
localparam [3:0] VALIDATE = 4'h1;

reg [3:0] push_state = 0;
reg [2:0] offset = 0;
reg flag = 0;
reg [N_DAC-1:0] ramp_rd_en_reg;
assign ramp_buf_rd_en = ramp_rd_en_reg;

always @( posedge clk_in ) begin
	case (push_state) 
		MOVE_DATA: begin
			if (buf_ramp_dv[(offset + 0)%8]) begin
				ramp_rd_en_reg[(offset + 0)%8] <= 1;
				buf_chan <= buf_ramp_chan[(offset + 0)%8];
				buf_data <= buf_ramp_data[(offset + 0)%8];
				data_valid <= 1;
				push_state <= VALIDATE;
			end //0
			else if (buf_ramp_dv[(offset + 1)%8]) begin
				ramp_rd_en_reg[(offset + 1)%8] <= 1;
				buf_chan <= buf_ramp_chan[(offset + 1)%8];
				buf_data <= buf_ramp_data[(offset + 1)%8];
				data_valid <= 1;
				push_state <= VALIDATE;
			end //1
			else if (buf_ramp_dv[(offset + 2)%8]) begin
				ramp_rd_en_reg[(offset + 2)%8] <= 1;
				buf_chan <= buf_ramp_chan[(offset + 2)%8];
				buf_data <= buf_ramp_data[(offset + 2)%8];
				data_valid <= 1;
				push_state <= VALIDATE;
			end //2
			else if (buf_ramp_dv[(offset + 3)%8]) begin
				ramp_rd_en_reg[(offset + 3)%8] <= 1;
				buf_chan <= buf_ramp_chan[(offset + 3)%8];
				buf_data <= buf_ramp_data[(offset + 3)%8];
				data_valid <= 1;
				push_state <= VALIDATE;
			end //3
			else if (buf_ramp_dv[(offset + 4)%8]) begin
				ramp_rd_en_reg[(offset + 4)%8] <= 1;
				buf_chan <= buf_ramp_chan[(offset + 4)%8];
				buf_data <= buf_ramp_data[(offset + 4)%8];
				data_valid <= 1;
				push_state <= VALIDATE;
			end //4
			else if (buf_ramp_dv[(offset + 5)%8]) begin
				ramp_rd_en_reg[(offset + 5)%8] <= 1;
				buf_chan <= buf_ramp_chan[(offset + 5)%8];
				buf_data <= buf_ramp_data[(offset + 5)%8];
				data_valid <= 1;
				push_state <= VALIDATE;
			end //5
			else if (buf_ramp_dv[(offset + 6)%8]) begin
				ramp_rd_en_reg[(offset + 6)%8] <= 1;
				buf_chan <= buf_ramp_chan[(offset + 6)%8];
				buf_data <= buf_ramp_data[(offset + 6)%8];
				data_valid <= 1;
				push_state <= VALIDATE;
			end //6
			else if (buf_ramp_dv[(offset + 7)%8]) begin
				ramp_rd_en_reg[(offset + 7)%8] <= 1;
				buf_chan <= buf_ramp_chan[(offset + 7)%8];
				buf_data <= buf_ramp_data[(offset + 7)%8];
				data_valid <= 1;
				push_state <= VALIDATE;
			end //7
			else begin 
				push_state <= MOVE_DATA;
				data_valid <= 0;
			end
			offset <= ((offset + 1)%8);
			chan <= buf_chan;
			data <= buf_data;
			
		end // MOVE_DATA state
		VALIDATE: begin
			data_valid <= 0;
			ramp_rd_en_reg[N_DAC-1:0] <= 8'b00000000;
			push_state <= MOVE_DATA;
		end // VALIDATE state
	endcase // case (push_state)
end //push data state machine


// set up ramp generating state machine
localparam [3:0] IDLE = 4'h0;
localparam [3:0] COUNT = 4'h1;
localparam [3:0] INCREMENT = 4'h2;

reg [3:0] state[0:N_DAC-1];
reg [3:0] prev_state[0:N_DAC-1];
integer k;
initial begin
	for (k = 0; k < N_DAC; k = k + 1) begin
		state[k] <= IDLE;
		prev_state[k] <= IDLE;
	end
end

integer j;
always @( posedge clk_in ) begin // this was negedge when things were working...5/1
	
	
	for (j = 0; j < N_DAC; j = j + 1) begin 
	//*****************************************************
		case (state[j]) 
			IDLE: begin
				current_dac_val[j] <= dac_min_val[j];
				next_dac_val[j] <= dac_min_val[j];
				chan_valid[j] <= 0;
				current_chan[j] <= 0;
				current_data[j] <= dac_min_val[j][15:0];
				if ( ~ramp_enable[j] ) begin
					counter[j] <= 0; // if the ramp is disabled this channel, reset counter 
					state[j] <= IDLE;
					if ( prev_state[j] == COUNT | prev_state[j] == INCREMENT ) begin
						current_chan[j] <= j[W_DAC_CHAN-1:0];
						current_data[j] <= dac_static_val[j];
						chan_valid[j] <= 1;
					end
				end
				else begin // ramp is enabled.
					state[j] <= COUNT;
					counter[j] <= 0;
				end
				prev_state[j] <= IDLE;
			end // end IDLE
			
			COUNT: begin
				current_dac_val[j] <= next_dac_val[j];
				chan_valid[j] <= 0;	
				if ( ramp_enable[j] ) begin
					if ( counter[j] < (cycles_per_step[j] - 2) ) begin // less 2 since it will not actually advance until the next clock cycle.
						counter[j] <= counter[j] + 1; 
						state[j] <= COUNT;
					end // increment the counter
					
					else if ( counter[j] == (cycles_per_step[j] - 2) ) begin
						state[j] <= INCREMENT;
						counter[j] <= 0;
					end // reached count, advance to increment value
					else begin
						state[j] <= COUNT;
						counter[j] <= 0;
					end // something weird happened; possibly cycles per step changed -> reset the counter
				end
				else begin // ramp disabled
					state[j] <= IDLE;
					counter[j] <= 0;
					chan_valid[j] <= 0;
				end
				prev_state[j] <= COUNT;
			end //state: COUNT
			
			
			INCREMENT: begin // increment counter and increment value
				if ( ramp_enable[j] ) begin
					// take action based on ramp direction:
					if (direction[j]) begin // going up
						if ( ( ((current_dac_val[j] + step_size[j]) < dac_max_val[j]) || ((current_dac_val[j] + step_size[j]) == dac_max_val[j]) ) && ( (current_dac_val[j] + step_size[j] > current_dac_val[j]) || (current_dac_val[j] + step_size[j] == current_dac_val[j]) ) ) begin // if within range
							next_dac_val[j] <= current_dac_val[j] + step_size[j];
							if ( (current_dac_val[j] + step_size[j]) == dac_max_val[j] ) begin
								direction[j] <= 1'b0; // if we're at the top, start going down next time
							end
						end
						else begin  // somehow we overshot or the max changed by request, reset to new max and go down
							direction[j] <= 1'b0;
							next_dac_val[j] <= dac_max_val[j]; 
						end
					end //end going up
					else begin // going down, direction[j] == 1'b0
						if ( ((current_dac_val[j] - step_size[j] > dac_min_val[j]) || (current_dac_val[j] - step_size[j] == dac_min_val[j]) ) && ( (current_dac_val[j] - step_size[j] < current_dac_val[j]) || (current_dac_val[j] - step_size[j] == current_dac_val[j]) ) ) begin // if within range
							next_dac_val[j] <= current_dac_val[j] - step_size[j];
							if ( (current_dac_val[j] - step_size[j]) == dac_min_val[j] ) begin
								direction[j] <= 1'b1; // if we're at the bottom, start going up next time
							end
						end
						else begin  // somehow we overshot or the min changed by request, reset to the new min and go up
							direction[j] <= 1'b1;
							next_dac_val[j] <= dac_min_val[j];
						end
					end // end going down
					
					chan_valid[j] <= 1'b1;
					current_chan[j] <= j[W_DAC_CHAN-1:0];
					current_data[j] <= next_dac_val[j];
					state[j] <= COUNT;
										
				end // if ramp_enable true
				else  begin // ramp_enable false
					state[j] <= IDLE;
				end
				prev_state[j] <= INCREMENT;
			end // case INCREMENT
		endcase // case(state[j])
	end //for
end // always


assign chan_out = chan;
assign data_out = data;
assign valid = data_valid;

endmodule

output_filter_boundsYes_initialSometimesRandom.v:
DDS lock works properly.
It is modified from the original version, add boundary check in stage 4 to limit the initial value. It still has the problem of random initial output when use addchannel function.

output_filter_boundsYes_noRandom_notLock.v:
DDS lock doesn't work.
Modified from output_filter_boundsYes_initialSometimesRandom.v. I moved add channel operation in stage 2, eliminate random init value problem, but fail to lock dds. I think the math of add channel part should be identical with original version.

output_filter_boundsYes_passDataStageByStage.v:
DDS lock works, but random init
based on output_filter_boundsYes_initialSometimesRandom.v. Seems no difference.

output_filter_boundsYes_addOneStage.v:
DDS lock works, but random init
add one stage to handle adding addchannel value(stage 6), but it still has random init values.
